
import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + sumar(num1, num2));
        System.out.println("Resta: " + restar(num1, num2));
        System.out.println("Multiplicació: " + multiplicar(num1, num2));
        System.out.println("Multiplicació: " + dividir(num1, num2));
    }
    
    public static double sumar (double a, double b) {
        return a + b;
    }

    public static double restar (double a, double b) {
        return a - b;
    }    
    
    public static double multiplicar (double a, double b) {
        return a * b;
    }    
    
    public static double dividir (double a, double b) throws ArithmeticException {
        if (b==a) {
            throw new ArithmeticException("Divisor és 0");
        }
        return a / b;
    } 
}